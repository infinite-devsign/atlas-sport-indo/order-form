<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    protected $data = [
        [
            'name' => 'Administrator',
            'email' => 'admin@mindiarydaily.com',
            'password' => 'Bismillah12345!'
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $datum) {
            \App\Entities\Accounts\User::query()->create($datum);
        }

        $this->command->info('Admin created');
    }
}
